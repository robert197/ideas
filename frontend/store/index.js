import Vuex from "vuex";
import GraphQLApi from "@/api/graphql";
import { ErrorHandler } from "@/util/ErrorHandler";

let graphQL = null;
const store = () => {
  return new Vuex.Store({
    state: {
      drawerIsOpen: false,
      ideas: [],
      user: null,
      selectedIdea: {},
      isLoading: false,
      ownIdeas: [],
      likeButtonAnimation: null,
      errorMessage: "",
      bookmarkedIdeas: {},
      contributedIdeas: {},
      dialog: {
        open: false,
        title: "",
        text: ""
      }
    },
    mutations: {
      toggleDrawer(state, isOpen) {
        state.drawerIsOpen = isOpen;
      },
      updateIdeasList(state, ideas) {
        state.ideas = ideas;
      },
      setUser(state, user) {
        state.user = user;
      },
      removeUser(state) {
        state.user = null;
      },
      setSelectedIdea(state, idea) {
        state.selectedIdea = idea;
      },
      loading(state, isLoading) {
        state.isLoading = isLoading;
      },
      updateOwnIdeasList(state, ownIdeas) {
        state.ownIdeas = ownIdeas;
      },
      setLikeButtonAnimation(state, animation) {
        state.likeButtonAnimation = animation;
      },
      setErrorMessage(state, errorMessage) {
        state.errorMessage = errorMessage;
      },
      updateBookmarkedIdeasList(state, bookmarkedIdeas) {
        state.bookmarkedIdeas = bookmarkedIdeas;
      },
      updateContributedIdeasList(state, contributedIdeas) {
        state.contributedIdeas = contributedIdeas;
      }
    },
    actions: {
      async initializeApp({ commit }) {
        graphQL = GraphQLApi(this.app.apolloProvider.defaultClient);
        const res = await graphQL.verifyToken();
        const isVerified =
          res.data && res.data.verifyToken && res.data.verifyToken.payload;
        if (isVerified) {
          const username = res.data.verifyToken.payload.username;
          let { data } = await graphQL.getUser(username);
          commit("setUser", data.user);
        }
      },
      async signUp({ commit }, user) {
        graphQL = GraphQLApi(this.app.apolloProvider.defaultClient);
        commit("loading", true);
        try {
          const { data } = await graphQL.createUser(user);
          const signedUp = data && data.createUser && data.createUser.user;
          commit("loading", false);
          if (signedUp) {
            commit("setUser", data.createUser.user);
          }
          return signedUp;
        } catch (e) {
          console.error(e);
          commit("loading", false);
          return false;
        }
      },

      async signIn({ commit }, user) {
        graphQL = GraphQLApi(this.app.apolloProvider.defaultClient);
        commit("loading", true);
        try {
          const { data } = await graphQL.tokenAuth(user);
          const signedIn = data && data.tokenAuth && data.tokenAuth.token;
          if (signedIn) {
            commit("setUser", data.tokenAuth.user);
          }
          commit("loading", false);
          return signedIn;
        } catch (e) {
          console.error(e);
          commit("loading", false);
          return false;
        }
      },

      signOut({ commit }) {
        commit("loading", true);
      },

      saveIdea({ commit, state }, idea) {
        commit("loading", true);
      },

      getIdea({ commit, state }, id) {
        commit("loading", true);
      },

      getIdeas({ commit }) {
        commit("loading", false);
      },

      getOwnIdeas({ commit }) {
        commit("loading", true);
      },

      getBookmarkedIdeas({ commit }) {
        commit("loading", true);
      },

      getContributedIdeas({ commit }) {
        commit("loading", true);
      },

      updateUserProfile({ commit }, profile) {
        commit("loading", true);
      },

      likeIdea({ commit, state }, idea) {},

      dislikeIdea({ commit, state }, idea) {},

      bookmarkIdea({ commit, state }, idea) {
        commit("loading", true);
      },

      removeBookmark({ commit, state }, idea) {
        commit("loading", true);
      },

      assignCurrentUserAsContributor({ commit, state }, idea) {
        commit("loading", true);
      }
    },
    getters: {
      isIdeaLikedByCurrentUser: state => idea => {
        return !!true;
      },
      isIdeaBookmarkedByCurrentUser: state => idea => {
        return !!true;
      },
      isCurrentUserContributorOfIdea: state => idea => {
        return !!true;
      },
      isOwnIdea: state => idea => {
        return !!true;
      }
    }
  });
};

export default store;
