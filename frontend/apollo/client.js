import { HttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";

// Replace this with your project's endpoint
const GQL_ENDPOINT = "http://localhost:8000/api/graphql";

export default () => ({
  link: new HttpLink({ uri: GQL_ENDPOINT }),
  cache: new InMemoryCache(),
  getAuth: () => {
    const token = localStorage.getItem("apollo-token");
    if (token) {
      return "JWT " + token;
    }
    return "";
  }
});
