const functions = require('firebase-functions');
const admin = require('firebase-admin')
admin.initializeApp()
const database = admin.database()

exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});

exports.createUserEntry = functions.auth.user().onCreate(user => {
  const newUser = {
    uid: user.uid,
    profile: {
      email: user.email
    }
  }
  return database.ref('users/' + user.uid).set(newUser)
});
