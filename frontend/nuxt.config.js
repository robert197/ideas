const nodeExternals = require("webpack-node-externals");

module.exports = {
  /*
   ** Build configuration
   */
  build: {
    vendor: ["vuetify"],
    extractCSS: true,
    extend(config, ctx) {
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/]
          })
        ];
      }
    }
  },
  /*
   ** Headers
   ** Common headers are already provided by @nuxtjs/pwa preset
   */
  head: {
    link: [
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons"
      },
      { src: "https://www.gstatic.com/firebasejs/5.2.0/firebase.js" }
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#e91e63", height: "5px" },
  /*
   ** Customize app manifest
   */
  manifest: {
    theme_color: "#3B8070"
  },
  /*
   ** Modules
   */
  modules: ["@nuxtjs/pwa", "@nuxtjs/apollo"],
  apollo: {
    clientConfigs: {
      default: "~/apollo/client.js"
    },
    tokenName: "apolloToken",
    includeNodeModules: true,
    authenticationType: "Bearer",
    defaultOptions: {
      $query: {
        loadingKey: "loading",
        fetchPolicy: "network"
      }
    }
  },
  plugins: ["~/plugins/vuetify"],
  css: ["~/assets/app.styl"]
};
