class References {
  constructor () {
    this.currentUser = null
  }

  ideas () {
    return '/ideas/'
  }

  idea (id) {
    return `/ideas/${id}/`
  }

  own () {
    return `/users/${this.getCurrentUser().uid}/`
  }

  ownIdeas () {
    return `/users/${this.getCurrentUser().uid}/ownIdeas/`
  }

  ownBookmarks () {
    return `/users/${this.getCurrentUser().uid}/bookmarks/`
  }

  ownUserProfile () {
    return `/users/${this.getCurrentUser().uid}/profile/`
  }

  ideasLikesAmount (id) {
    return `/ideas/${id}/likes/amount/`
  }

  likedByCurrentUser (id) {
    return `/ideas/${id}/likes/likedBy/${this.getCurrentUser().uid}/`
  }

  bookmarkedByCurrentUser (id) {
    return `/ideas/${id}/bookmarks/bookmarkedBy/${this.getCurrentUser().uid}/`
  }

  ideasContributors (id) {
    return `/ideas/${id}/contributors/`
  }

  ownContributions () {
    return `/users/${this.getCurrentUser().uid}/contributions/`
  }

  setCurrentUser (user) {
    if (!user.hasOwnProperty('uid')) {
      throw new TypeError('uid inside user object is missing!')
    }
    this.currentUser = user
  }

  getCurrentUser () {
    this._checkIfCurrentUserExists()
    return this.currentUser
  }

  _checkIfCurrentUserExists () {
    if (!this.currentUser) {
      throw new Error('current user is not set')
    }
  }

}

export default References
