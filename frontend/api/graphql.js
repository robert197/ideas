import gql from "graphql-tag";

const api = apolloClient => {
  return {
    async createUser(user) {
      return await apolloClient.mutate({
        mutation: gql`
          mutation createUser(
            $email: String!
            $firstName: String!
            $lastName: String!
            $password: String!
            $username: String!
          ) {
            createUser(
              email: $email
              firstName: $firstName
              lastName: $lastName
              password: $password
              username: $username
            ) {
              user {
                id
                email
                firstName
                lastName
                username
              }
            }
          }
        `,
        variables: {
          ...user
        }
      });
    },
    async tokenAuth(credentials) {
      return await apolloClient.mutate({
        mutation: gql`
          mutation tokenAuth($username: String!, $password: String!) {
            tokenAuth(username: $username, password: $password) {
              token
              user {
                id
                email
                firstName
                lastName
                username
              }
            }
          }
        `,
        variables: {
          ...credentials
        }
      });
    },
    async verifyToken() {
      return await apolloClient.mutate({
        mutation: gql`
          mutation verifyToken($token: String!) {
            verifyToken(token: $token) {
              payload
            }
          }
        `,
        variables: {
          token: localStorage.getItem("apollo-token")
        }
      });
    },
    async getUser(username) {
      return await apolloClient.query({
        query: gql`
          query user($username: String!) {
            user(username: $username) {
              id
              email
              firstName
              lastName
              username
            }
          }
        `,
        variables: {
          username
        }
      });
    }
  };
};

export default api;
