import References from "./references";

class Api extends References {
  constructor(firebase) {
    super();
    this.db = firebase.database();
    this.auth = firebase.auth();
  }

  onIdeasChange(cb, err) {
    return this.db
      .ref(this.ideas())
      .orderByKey()
      .on("value", cb, err);
  }

  onOwnIdeasChange(cb, err) {
    return this.db
      .ref(this.ownIdeas())
      .orderByKey()
      .on("value", cb, err);
  }

  onIdeaChange(id, cb, err) {
    return this.db.ref(this.idea(id)).on("value", cb, err);
  }

  onBookmarkedIdeasChange(cb, err) {
    return this.db
      .ref(this.ownBookmarks())
      .orderByKey()
      .on("value", cb, err);
  }

  onOwnContributedIdeasChange(cb, err) {
    let contributions = [];
    const onContrib = c => {
      contributions.push(c.val());
    };

    this.db.ref(this.ownContributions()).on("value", data => {
      const contributionIds = Object.keys(data.val());
      contributionIds.forEach(id => {
        return this.db.ref(this.ideas() + id).on("value", onContrib, err);
      });
      cb(contributions);
    });
    contributions = [];
  }

  createUser(user) {
    return this.auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(auth => {
        this.setCurrentUser(auth.user);
        return auth;
      });
  }

  signInUser(user) {
    return this.auth
      .signInWithEmailAndPassword(user.email, user.password)
      .then(auth => {
        this.setCurrentUser(auth.user);
        return auth;
      });
  }

  onAuthStateChange(cb) {
    return this.auth.onAuthStateChanged(user => {
      if (user) {
        this.setCurrentUser(user);
        cb(user);
      } else {
        cb(null, new Error("No user signed in."));
      }
    });
  }

  signOut() {
    this.auth.signOut();
  }

  saveCurrentUserProfile(profile) {
    const user = this.getCurrentUser();
    user.updateProfile(profile);
    this.setCurrentUser(user);
    return this.db
      .ref(this.ownUserProfile())
      .set({ uid: user.uid, email: user.email, ...profile });
  }

  increaseIdeasLikes(idea) {
    let update = {};
    const likesAmount = idea.likes.amount + 1;
    const user = this.getCurrentUser();
    update[this.ideasLikesAmount(idea.id)] = likesAmount;
    update[this.likedByCurrentUser(idea.id)] = {
      email: user.email,
      displayName: user.displayName
    };
    return this.db.ref().update(update);
  }

  decreaseIdeasLikes(idea) {
    let update = {};
    const likesAmount = idea.likes.amount - 1;
    update[this.ideasLikesAmount(idea.id)] = likesAmount;
    update[this.likedByCurrentUser(idea.id)] = null;
    return this.db.ref().update(update);
  }

  saveIdea(idea, owner) {
    let update = {};
    const key = this.db
      .ref()
      .child("ideas")
      .push().key;
    const newIdea = {
      ...idea,
      owner,
      bookmarks: { bookmarkedBy: {} },
      likes: { likedBy: {}, amount: 0 }
    };
    update[this.ideas() + key] = newIdea;
    update[this.ownIdeas() + key] = newIdea;
    return this.db.ref().update(update);
  }

  bookmarkIdea(idea) {
    let update = {};
    const user = this.getCurrentUser();
    const userProfile = { email: user.email, displayName: user.displayName };
    let bookmarkedIdea = {
      ...idea,
      bookmarks: {
        bookmarkedBy: {}
      }
    };
    bookmarkedIdea.bookmarks.bookmarkedBy[user.uid] = userProfile;
    update[this.ownBookmarks() + idea.id] = bookmarkedIdea;
    update[this.bookmarkedByCurrentUser(idea.id)] = userProfile;
    return this.db.ref().update(update);
  }

  removeBookmark(idea) {
    let update = {};
    update[this.ownBookmarks() + idea.id] = null;
    update[this.bookmarkedByCurrentUser(idea.id)] = null;
    return this.db.ref().update(update);
  }

  assignCurrentUserAsContributor(idea) {
    let update = {};
    const user = this.getCurrentUser();
    const userProfile = { email: user.email, displayName: user.displayName };
    update[this.ideasContributors(idea.id) + user.uid] = userProfile;
    update[this.ownContributions() + idea.id] = idea.id;
    return this.db.ref().update(update);
  }
}

export default Api;
