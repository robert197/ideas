import * as firebase from 'firebase'

if (!firebase.apps.length) {
  const config = {
    apiKey: "AIzaSyCLMHWiO45aKVcpw-QR7POtMdwrRN8gO4w",
    authDomain: "ideas-ac014.firebaseapp.com",
    databaseURL: "https://ideas-ac014.firebaseio.com",
    projectId: "ideas-ac014",
    storageBucket: "ideas-ac014.appspot.com",
    messagingSenderId: "243960058825"
  };
  firebase.initializeApp(config);
}

export default firebase
