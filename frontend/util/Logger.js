export class Logger {
  info (i) {
    console.log(i)
  }

  warning (w) {
    console.warning(w)
  }

  error (e) {
    console.error(e)
  }
}
