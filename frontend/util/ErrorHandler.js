import { Logger } from '@/util/Logger.js'

export class ErrorHandler {

  constructor ({ errorMutation, loadingMutation }) {
    this.logger = new Logger()
    this.errorMutation = errorMutation
    this.loadingMutation = loadingMutation
    this.errorMessage = null
  }

  handle (error) {
    this.errorMessage = error.message
    this.logger.error(error)
    return this
  }

  commit (commit) {
    if (!this.errorMessage) {
      throw new Error('Call handle function before commit!')
    }
    commit(this.errorMutation, this.errorMessage)
    commit(this.loadingMutation, false)
  }
}
