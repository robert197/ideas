kubectl get secret $(kubectl get secrets | grep 'default-token' | awk '/default-token/ {print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
