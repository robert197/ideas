from rest_framework.response import Response
from rest_framework.test import APIClient

from utils.baseTest import BaseTest


class AuthenticationTest(BaseTest):
    def test_LoginReturnsTokenOnSuccess(self):
        client = APIClient()
        response: Response = client.post("/api/token/", self.credentials)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.getPropertyFromResponse(response, 'access').startswith('ey'))

    def test_LoginFailsWhenNoCredentialsAreProvided(self):
        client = APIClient()
        response: Response = client.post("/api/token/")
        self.assertEqual(response.status_code, 400)
        self.assertJSONEqual(response.rendered_content, {'username': ['This field is required.'],
                                                         'password': ['This field is required.']})

    def test_LoginFailsWhenWrongCredentialsAreProvided(self):
        client = APIClient()
        response: Response = client.post("/api/token/", {"username": "admin", "password": "wrong password"})
        self.assertEqual(response.status_code, 401)
        self.assertJSONEqual(response.rendered_content,
                             {'detail': 'No active account found with the given credentials'})