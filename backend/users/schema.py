import graphene
from graphene_django.types import DjangoObjectType
from django.contrib.auth import get_user_model
from graphql_jwt.decorators import login_required
from graphql_jwt import JSONWebTokenMutation, Verify, Refresh
from graphql_jwt.utils import get_payload

User = get_user_model()


class UserType(DjangoObjectType):
    class Meta:
        model = User
        exclude_fields = ['password', 'is_active']


class Query(graphene.AbstractType):
    users = graphene.List(UserType)
    user = graphene.Field(UserType, username=graphene.String())

    @staticmethod
    @login_required
    def resolve_users(self, info, **kwargs):
        return User.objects.filter(is_active=True)

    @staticmethod
    @login_required
    def resolve_user(self, info, username, **kwargs):
        return User.objects.filter(is_active=True).get(username=username)


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        username = graphene.String(required=True)
        password = graphene.String(required=True)
        email = graphene.String(required=True)
        first_name = graphene.String(required=False)
        last_name = graphene.String(required=False)

    def mutate(self, info, username, password, email, first_name, last_name):
        user = User.objects.create_user(
            username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        user.save()
        return CreateUser(user=user)


class ObtainJSONWebToken(JSONWebTokenMutation):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)


class VerifyToken(Verify):
    user = graphene.Field(UserType)

    class Arguments:
        token = graphene.String(required=True)

    @classmethod
    def mutate(cls, root, info, token, **kwargs):
        import logging
        logger = logging.getLogger(__name__)
        logger.warning('###############')
        logger.warning(info.context.user)
        return cls(payload=get_payload(token, info.context))


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    token_auth = ObtainJSONWebToken.Field()
    verify_token = Verify.Field()
    refresh_token = Refresh.Field()
