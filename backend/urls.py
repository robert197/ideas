"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from swagger_schema import SwaggerSchemaView
from graphql_jwt.decorators import jwt_cookie


urlpatterns = [
    url(r'^api/', include([
        re_path(r'^$', SwaggerSchemaView.as_view()),
        re_path(r'^graphql', csrf_exempt(jwt_cookie(GraphQLView.as_view(graphiql=True)))),
        re_path(r'^token/$', TokenObtainPairView.as_view(), name='token_obtain_pair'),
        re_path(r'^token/refresh/$', TokenRefreshView.as_view(), name='token_refresh'),
        path('users/', include('users.urls')),
        path('admin/', admin.site.urls),
        path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    ]))
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
