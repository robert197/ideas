import graphene

from users import schema as users_schema
from ideas import schema as ideas_schema
from comments import schema as comments_schema


class Query(users_schema.Query, ideas_schema.Query, comments_schema.Query, graphene.ObjectType):
    pass


class Mutation(users_schema.Mutation, ideas_schema.Mutation, comments_schema.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
