from django.db import models
from django.contrib.auth.models import User


class Comment(models.Model):
    text = models.TextField(blank=False, null=False)
    idea = models.ForeignKey('ideas.idea', on_delete=models.CASCADE, default=666)
    answers = models.ManyToManyField('comments.comment', null=True, blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
