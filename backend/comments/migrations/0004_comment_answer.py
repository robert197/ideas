# Generated by Django 2.0.4 on 2019-06-25 23:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('comments', '0003_comment_idea'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='answer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='comments.Comment'),
        ),
    ]
