import graphene
from graphene_django.types import DjangoObjectType
from .models import Comment
from ideas.models import Idea


class CommentType(DjangoObjectType):
    class Meta:
        model = Comment


class Query(graphene.AbstractType):
    comments = graphene.List(CommentType)

    def resolve_comments(self, info, **kwargs):
        return Comment.objects.all()

    def resolve_comment(self, info, id, **kwargs):
        return Comment.objects.get(pk=id)


class CreateComment(graphene.Mutation):
    comment = graphene.Field(CommentType)

    class Arguments:
        text = graphene.String(required=True)
        idea = graphene.ID(required=True)

    def mutate(self, info, text, idea):
        idea = Idea.objects.get(pk=idea)
        created_by = info.context.user
        comment = Comment(
            text=text,
            idea=idea,
            created_by=created_by
        )
        comment.save()
        return CreateComment(comment=comment)


class CreateAnswer(graphene.Mutation):
    answer = graphene.Field(CommentType)
    comment = graphene.Field(CommentType)

    class Arguments:
        comment = graphene.ID(required=True)
        text = graphene.String(required=True)
        idea = graphene.ID(required=True)

    def mutate(self, info, comment, text, idea):
        idea = Idea.objects.get(pk=idea)
        created_by = info.context.user

        answer = Comment(
            text=text,
            idea=idea,
            created_by=created_by
        )
        answer.save()

        comment: Comment = Comment.objects.get(pk=comment)
        comment.answers.add(answer)


class Mutation(graphene.ObjectType):
    create_comment = CreateComment.Field()
    createAnswer = CreateAnswer.Field()
