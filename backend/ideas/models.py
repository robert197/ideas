from django.db import models
from django.contrib.auth.models import User

from comments.models import Comment


class Idea(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=False, null=False)
    liked_by = models.ManyToManyField(User, related_name='liked_by', blank=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='created_by')
    bookmarked_by = models.ManyToManyField(User, related_name='bookmarked_by', blank=True)
    contributors = models.ManyToManyField(User, related_name='contributors', blank=True)
    comments = models.ManyToManyField(Comment, related_name='comments', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def likes_amount(self):
        return self.liked_by.count()

    @property
    def contributors_amount(self):
        return self.contributors.count()


class Bookmark(models.Model):
    idea = models.ForeignKey(Idea, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
