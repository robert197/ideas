import graphene
import logging
from graphene_django.types import DjangoObjectType
from .models import Idea, Bookmark

logger = logging.getLogger(__name__)


class IdeaType(DjangoObjectType):
    likes_amount = graphene.Int()
    contributors_amount = graphene.Int()

    class Meta:
        model = Idea


class BookmarkType(DjangoObjectType):
    class Meta:
        model = Bookmark


class Query(graphene.AbstractType):
    ideas = graphene.List(IdeaType)
    own_ideas = graphene.List(IdeaType)
    idea = graphene.Field(IdeaType, id=graphene.Int())
    bookmarks = graphene.List(BookmarkType)

    def resolve_ideas(self, info, **kwargs):
        return Idea.objects.all()

    def resolve_own_ideas(self, info, **kwargs):
        return Idea.objects.filter(created_by=info.context.user)

    def resolve_idea(self, info, id, **kwargs):
        return Idea.objects.get(pk=id)

    def resolve_bookmarks(self, info, **kwargs):
        return Bookmark.objects.filter(user=info.context.user)


class CreateIdea(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        name = graphene.String(required=True)
        description = graphene.String(required=True)

    def mutate(self, info, name, description):
        idea = Idea(
            name=name,
            description=description,
            created_by=info.context.user
        )
        idea.save()
        return CreateIdea(idea=idea)


class LikeIdea(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        liked_by = info.context.user
        is_already_liked_by_user = Idea.objects.filter(pk=idea, liked_by=liked_by.id).exists()
        if is_already_liked_by_user:
            raise Exception('User already liked this idea')
        idea = Idea.objects.get(pk=idea)
        idea.liked_by.add(liked_by.id)
        idea.save()
        return LikeIdea(idea=idea)


class DislikeIdea(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        liked_by = info.context.user
        is_already_liked_by_user = Idea.objects.filter(pk=idea, liked_by=liked_by).exists()
        if not is_already_liked_by_user:
            raise Exception('User has not liked this idea yet')
        idea = Idea.objects.get(pk=idea)
        idea.liked_by.remove(liked_by)
        idea.save()
        return LikeIdea(idea=idea)


class BookmarkIdea(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        bookmarked_by = info.context.user
        if Idea.objects.filter(pk=idea, bookmarked_by=bookmarked_by).exists():
            raise Exception('Idea has been already bookmarked')
        idea = Idea.objects.get(pk=idea)
        bookmark = Bookmark(
            idea=idea,
            user=info.context.user
        )
        idea.bookmarked_by.add(bookmarked_by.id)
        bookmark.save()
        return BookmarkIdea(idea=idea)


class UnbookmarkIdea(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        if not Bookmark.objects.filter(idea__pk=idea).exists():
            raise Exception('Bookmark with this id was not found')
        bookmarked_by = info.context.user
        bookmark = Bookmark.objects.get(idea__pk=idea)
        bookmark.idea.bookmarked_by.remove(bookmarked_by)
        idea = bookmark.idea
        bookmark.delete()
        return UnbookmarkIdea(idea=idea)


class Contribute(graphene.Mutation):
    idea = graphene.Field(IdeaType)

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        contributor = info.context.user
        if Idea.objects.filter(pk=idea, contributors=contributor).exists():
            raise Exception('User has already contributed to this idea')
        idea = Idea.objects.get(pk=idea)
        idea.contributors.add(contributor)
        idea.save()
        return Contribute(idea=idea)


class UnContribute(graphene.Mutation):
    ok = graphene.Boolean()

    class Arguments:
        idea = graphene.ID()

    def mutate(self, info, idea):
        contributor = info.context.user
        if not Idea.objects.filter(pk=idea, contributors=contributor).exists():
            raise Exception('User has not contributed to this idea yet')
        idea = Idea.objects.get(pk=idea)
        idea.contributors.remove(contributor)
        idea.save()
        return UnContribute(ok=True)


class Mutation(graphene.ObjectType):
    create_idea = CreateIdea.Field()
    like_idea = LikeIdea.Field()
    dislike_idea = DislikeIdea.Field()
    bookmark_idea = BookmarkIdea.Field()
    unbookmark_idea = UnbookmarkIdea.Field()
    contribute = Contribute.Field()
    uncontribute = UnContribute.Field()
