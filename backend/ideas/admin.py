from django.contrib import admin
from .models import Idea, Bookmark

admin.site.register(Idea)
admin.site.register(Bookmark)
