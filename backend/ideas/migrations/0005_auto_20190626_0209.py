# Generated by Django 2.0.4 on 2019-06-26 00:09

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ideas', '0004_auto_20190625_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='idea',
            name='liked_by',
            field=models.ManyToManyField(blank=True, related_name='liked_by', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='idea',
            name='likes',
            field=models.IntegerField(default=0),
        ),
    ]
