#!/bin/bash

function waitForPostgres() {
    echo "Waiting for postgres ..."
    python wait_for_postgres.py
}

function migrateTables() {
    echo "Migrating tables..."
    python manage.py makemigrations && python manage.py migrate
}

function loadFixtures() {
    echo "Loading fixtures..."
    if [[ ${ENVIRONMENT} == "development" ]]; then
        python manage.py loaddata 02_AccessTokens_development
    else
        python manage.py loaddata 02_AccessTokens
    fi
}

function createSuperuser () {
    echo "Creating superuser..."
    local username="$1"
    local password="$2"
    python manage.py create_superuser $username $password
}


waitForPostgres
migrateTables
if [[ ${ENVIRONMENT} != "production" ]]; then
    createSuperuser "admin" "admin123"
fi
loadFixtures

python manage.py runserver 0.0.0.0:8000
#gunicorn --bind 0.0.0.0:8000 --reload --access-logfile - wsgi:application
